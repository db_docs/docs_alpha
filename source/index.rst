.. First doc documentation master file, created by
   sphinx-quickstart on Thu Jun  5 08:32:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to Odoo version 8’s documentation!
==========================================

Introduction
------------
OpenERP is a Management Software which is flexible, simple and complete solution
that can be used in various sectors and trades, including agricultural products,
textiles, public auctions, IT, and trade associations.
So you will find modules to suit all kinds of needs, allowing your company to
build its customized system by simply grouping and configuring the most suitable
modules : Employees Directory , Recruitment, Accounting, Billing, Project management,
Warehouse management, CRM, Point of Sales... 

Contents
--------

.. toctree::
   :maxdepth: 2
   
   product_variant_cost   
   product_price_history 
   sale_pricelist_items
   product_variant_location.rst
   contact 
   