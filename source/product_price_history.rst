Product Price History
=====================
  
Module
------
Name: *product_price_history*

version: 1.2.3

Features
--------

This module allows you to:

- Record various prices of the same product for different companies. This
  way, every company can have its own costs (average or standard) and
  sale prices.
- Historize the prices in a way that you'll then be able to retrieve the
  cost price at a given date.


Print Screen
------------

1°- view history prices:

**Menu : Sales-->Products--> Product Variants** 

Choose any product variant :


.. figure:: _static/imgs/history1.png
   :align: center


* 1 : click on Price history.


.. figure:: _static/imgs/history2.png
   :width: 800
   :align: center
 

you can also see the Price history for all product variants.

**Menu : Sales-->Products--> Price history by product** 

.. figure:: _static/imgs/history3.png
   :width: 800
   :align: center
   
   
with  *purchase_product_price_history* module you can see the Price history in 
Purchase section.

**Menu : Purchases-->Products--> Price history by product** 

.. figure:: _static/imgs/history4.png
   :width: 800
   :align: center
   