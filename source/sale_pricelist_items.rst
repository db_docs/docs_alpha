Managing Price Lists
====================
  
Module
------
Name: *sale_pricelist_items*

version: 1.1

Features
--------

This module allows you to:

- Easily export/import PriceList to update a sale price.
- Update Pricelist Price from Quote .

Print Screen
------------

A price list contains rules to be evaluated in order to compute the sales price 
of the products.
Price lists may have several versions (2010, 2011, Promotion of February 2010, etc.)
and each version may have several rules. (e.g. the customer price of a product
category will be based on the supplier price multiplied by 1.80). 

1°- Import/Export PriceList:

**Export PriceList**

To make easy the export and import PriceList the new menu is added to display all 
rules grouped by  Price lists versions.

**Menu : Sales-->Configuration-->Pricelists -->Pricelist items** 

Choose the products you want to set a new value for sale price.

Then select option *More-->Export** 

.. figure:: _static/imgs/pricelist1.png
   :width: 800
   :align: center


* 1 : Choose a template *Export PriceList*

* 2 : Click Export to file.


.. figure:: _static/imgs/pricelist2.png
   :width: 800
   :align: center
   
   
a csv file will be generated with the columns [id,default_code,name,sale_price]
       
       
.. figure:: _static/imgs/pricelist3.png
   :width: 800
   :align: center


**Import PriceList**
  
**Menu : Sales-->Configuration-->Pricelists -->Pricelist items**

Click on the option Import.


.. figure:: _static/imgs/pricelist4.png
   :width: 800
   :align: center

   
**Update Pricelist Price from Quote**
 
 use button Update Quote Price from Pricelist in sale order form.
 
.. figure:: _static/imgs/pricelist5.png
   :width: 800
   :align: center