Product Variant Cost
====================

Module
------
Name: *product_variant_cost*

version: 1.0

Features
--------
- Add a new cost field in product variant.
- Updates the product variant's cost when product template cost is updated.
- Adds a new inventory field on quants, (product variant cost * quant quantity).


Print Screen
------------
[anajuaristi]
This is due Odoo core is not including cost_price on specific variants. Field is
stored on template so with this aproach all variants from a template should have same price.
This causes several lateral efects on product cost history, inventory valuation,
manufacturing orders cost an so on.
To minimize a little bit those efects we added the field cost price on specific
variant product form and we did a module that allows including different cost
prices on variants.


1°-Set the cost price for each product variant:

**Menu : Sales-->Products--> Product Variants** 


.. figure:: _static/imgs/image1.png
   :align: center



* 1 : The new fields added you can change it anytime.
 
* 2 : This is a old field cost(is not removed used in inventory valuation and other)
       but if you set value in new field it takes the same value.
       
2°-Use a tool export/import to set the cost price :

   **Export Product Variants**

**Menu : Sales-->Products--> Product Variants** 

Choose the products you want to set a new value for cost price.

Then select option *More-->Export** 


.. figure:: _static/imgs/image2.png
   :align: center
   
* 1 : Choose a template *Export Cost Price*

* 2 : Click Export to file.
       a csv file will be generated with the columns [id,default_code,name,cost_price]
       
       
.. figure:: _static/imgs/image3.png
   :align: center


**Import Product Variants**
  
**Menu : Sales-->Products--> Product Variants** 

Click on the option Import.


.. figure:: _static/imgs/image4.png
   :align: center
